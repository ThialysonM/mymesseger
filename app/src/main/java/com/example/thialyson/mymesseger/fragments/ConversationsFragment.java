package com.example.thialyson.mymesseger.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import com.example.thialyson.mymesseger.ConversationActivity;
import com.example.thialyson.mymesseger.ConversationAdapter;
import com.example.thialyson.mymesseger.R;
import com.example.thialyson.mymesseger.model.Contact;
import com.example.thialyson.mymesseger.model.Conversation;
import com.example.thialyson.mymesseger.util.Base64Util;
import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.example.thialyson.mymesseger.util.PreferencesUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConversationsFragment extends Fragment {

    public ListView listView;
    public ArrayList<Conversation> conversations;
    public DatabaseReference firebase;
    public ValueEventListener valueEventListenerConversations;
    public ConversationAdapter adapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_conversations, container, false);

        conversations = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.lvConversationsFragment);

        adapter = new ConversationAdapter(getActivity(), conversations);
        listView.setAdapter(adapter);
        listenItemClick();

        firebase = ConfigFirebase.getFirebase().child("conversations").child(PreferencesUtil.getPref("idUser", getActivity()));
        setValueEventListener();

        return view;
    }

    void setValueEventListener() {
        valueEventListenerConversations = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                conversations.clear();

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Conversation conversation = data.getValue(Conversation.class);
                    conversations.add(conversation);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    void listenItemClick() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Conversation conversation = conversations.get(position);
                Intent intent = new Intent(getActivity(), ConversationActivity.class);
                intent.putExtra("name", conversation.getName());
                intent.putExtra("email", Base64Util.decode(conversation.getUserId()));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        firebase.addValueEventListener(valueEventListenerConversations);
    }

    @Override
    public void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListenerConversations);
    }
}
