package com.example.thialyson.mymesseger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thialyson.mymesseger.model.Contact;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by thialyson on 23/05/17.
 */

public class ContactAdapter extends BaseAdapter {

    private List<Contact> contacts;
    private Context context;

    public ContactAdapter(Context context, List<Contact> contacts) {
        this.contacts = contacts;
        this.context = context;
    }

    @Override
    public int getCount() {
        return contacts.size();
    }

    @Override
    public Object getItem(int position) {
        return contacts.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private class MyViewHolder {
        ImageView avatarContact;
        TextView nameContact, emailContact;

        MyViewHolder(View v) {
//            avatarContact = (ImageView) v.findViewById(R.id.avatarContact);
            nameContact = (TextView) v.findViewById(R.id.nameContact);
            emailContact = (TextView) v.findViewById(R.id.emailContact);
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        MyViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item_contact, parent, false);
            holder = new MyViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (MyViewHolder) row.getTag();
        }

        Contact contact = contacts.get(position);

        holder.nameContact.setText(contact.getName());
        holder.emailContact.setText(contact.getEmail());

//        Picasso.with(context).load(ShieldUtil.getShield(String.valueOf(match.getTeamHomeId()))).fit().into(holder.imageHomeTeam);

        return row;
    }
}
