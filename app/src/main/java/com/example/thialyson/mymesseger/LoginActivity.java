package com.example.thialyson.mymesseger;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thialyson.mymesseger.model.User;
import com.example.thialyson.mymesseger.util.Base64Util;
import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.example.thialyson.mymesseger.util.PreferencesUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthInvalidUserException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class LoginActivity extends AppCompatActivity {

    private EditText email, password;
    private Button btEnter;
    private User user;
    private DatabaseReference firebase;
    private ValueEventListener valueEventListenerLogin;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        FirebaseAuth auth = ConfigFirebase.getAuth();

        if (auth.getCurrentUser() != null) {
            callHomeActivity();

        } else {
            email = (EditText) findViewById(R.id.etEmail);
            password = (EditText) findViewById(R.id.etPassword);
            btEnter = (Button) findViewById(R.id.btEnter);

            listenBtRegisterClick();
        }
    }

    private void listenBtRegisterClick() {
        btEnter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                user = new User();
                user.setEmail(email.getText() != null ? email.getText().toString() : "");
                user.setPassword(password.getText() != null ? password.getText().toString() : "");

                validateFields();
            }
        });
    }

    private void validateFields() {
        if (!user.getEmail().equals("") && !user.getPassword().equals("")) {
            login();
        } else {
            showToast("Todos os campos são obrigatórios!");
        }
    }

    private void login() {
        FirebaseAuth auth = ConfigFirebase.getAuth();
        auth.signInWithEmailAndPassword(
                user.getEmail(),
                user.getPassword()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {

                    String idUser = Base64Util.encode(user.getEmail());

                    firebase = ConfigFirebase.getFirebase().child("users").child(idUser);
                    setValueEventListener();
                    firebase.addListenerForSingleValueEvent(valueEventListenerLogin);

                    PreferencesUtil.setPref("idUser", idUser , LoginActivity.this);

                    callHomeActivity();
                } else {
                    resolveExceptions(task);
                }
            }
        });
    }

    void setValueEventListener(){
        valueEventListenerLogin = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                User user = dataSnapshot.getValue(User.class);
                PreferencesUtil.setPref("nameUser", user.getName() , LoginActivity.this);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    private void resolveExceptions(Task<AuthResult> task) {
        try {
            throw task.getException();
        } catch (FirebaseAuthInvalidUserException e) {
            showToast("Email não encontrado!");
        } catch (FirebaseAuthInvalidCredentialsException e) {
            showToast("Senha incorreta!");
        } catch (FirebaseException e) {
            showToast("Sem conexão com a internet!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void callHomeActivity(){
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
        finish();
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    public void goToRegister(View view) {
        startActivity(new Intent(this, RegisterActivity.class));
    }


}
