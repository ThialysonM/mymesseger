package com.example.thialyson.mymesseger.model;

import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.Exclude;

/**
 * Created by thialyson on 23/04/17.
 */

public class User {

    private String id, name, email, password;

    public User() {

    }

    public void save() {
        DatabaseReference reference = ConfigFirebase.getFirebase();
        reference.child("users").child(getId()).setValue(this);
    }

    @Exclude
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Exclude
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
