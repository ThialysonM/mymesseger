package com.example.thialyson.mymesseger.model;

/**
 * Created by thialyson on 24/04/17.
 */

public class Contact {

    private String userId, name, email;


    public Contact() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
