package com.example.thialyson.mymesseger.util;

import android.util.Base64;

/**
 * Created by thialyson on 23/04/17.
 */

public class Base64Util {

    public static String encode(String text) {
        return Base64.encodeToString(text.getBytes(), Base64.DEFAULT).replaceAll("(\\n|\\r)", "");
    }

    public static String decode(String text) {
        return new String(Base64.decode(text.getBytes(), Base64.DEFAULT));
    }
}
