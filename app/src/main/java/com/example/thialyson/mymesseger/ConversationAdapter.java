package com.example.thialyson.mymesseger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.thialyson.mymesseger.model.Conversation;

import java.util.List;

/**
 * Created by thialyson on 24/05/17.
 */

public class ConversationAdapter extends BaseAdapter {
    private List<Conversation> conversations;
    private Context context;

    public ConversationAdapter(Context context, List<Conversation> conversations) {
        this.conversations = conversations;
        this.context = context;
    }

    @Override
    public int getCount() {
        return conversations.size();
    }

    @Override
    public Object getItem(int position) {
        return conversations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private class MyViewHolder {
        ImageView avatarContact;
        TextView nameContact, lastMessage;

        MyViewHolder(View v) {
//            avatarContact = (ImageView) v.findViewById(R.id.avatarContact);
            nameContact = (TextView) v.findViewById(R.id.tvNameContact);
            lastMessage = (TextView) v.findViewById(R.id.lastMessage);
        }
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View row = convertView;
        ConversationAdapter.MyViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.item_conversation, parent, false);
            holder = new ConversationAdapter.MyViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (ConversationAdapter.MyViewHolder) row.getTag();
        }

        Conversation conversation = conversations.get(position);

        holder.nameContact.setText(conversation.getName());
        holder.lastMessage.setText(conversation.getMessage());

//        Picasso.with(context).load(ShieldUtil.getShield(String.valueOf(match.getTeamHomeId()))).fit().into(holder.imageHomeTeam);

        return row;
    }
}
