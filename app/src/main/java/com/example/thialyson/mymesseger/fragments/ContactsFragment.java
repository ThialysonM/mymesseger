package com.example.thialyson.mymesseger.fragments;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.thialyson.mymesseger.ContactAdapter;
import com.example.thialyson.mymesseger.ConversationActivity;
import com.example.thialyson.mymesseger.R;
import com.example.thialyson.mymesseger.model.Contact;
import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.example.thialyson.mymesseger.util.PreferencesUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class ContactsFragment extends Fragment {

    public ListView listView;
    public ArrayList<Contact> contacts;
    public DatabaseReference refFirebase;
    public ValueEventListener valueEventListener;
    public ContactAdapter contactAdapter;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_contacts, container, false);

        contacts = new ArrayList<>();
        listView = (ListView) view.findViewById(R.id.lvContacts);

        contactAdapter = new ContactAdapter(getActivity(), contacts);
        listView.setAdapter(contactAdapter);

        refFirebase = ConfigFirebase.getFirebase()
                .child("contacts")
                .child(PreferencesUtil.getPref("idUser", getActivity()));

        valueEventListener();

        listenItemClick();

        return view;
    }

    void valueEventListener() {
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                contacts.clear();
                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Contact contact = data.getValue(Contact.class);
                    contacts.add(contact);
                }
                contactAdapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    void listenItemClick() {
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                Contact contact = contacts.get(position);
                Intent intent = new Intent(getActivity(), ConversationActivity.class);
                intent.putExtra("name", contact.getName());
                intent.putExtra("email", contact.getEmail());
                startActivity(intent);
            }
        });
    }

    @Override
    public void onStart() {
        super.onStart();
        refFirebase.addValueEventListener(valueEventListener);
    }

    @Override
    public void onStop() {
        super.onStop();
        refFirebase.removeEventListener(valueEventListener);
    }

}
