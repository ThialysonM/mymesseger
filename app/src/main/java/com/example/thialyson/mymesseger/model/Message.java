package com.example.thialyson.mymesseger.model;

/**
 * Created by thialyson on 23/05/17.
 */

public class Message {

    private String userId, message;

    public Message() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
