package com.example.thialyson.mymesseger;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.thialyson.mymesseger.model.Conversation;
import com.example.thialyson.mymesseger.model.Message;
import com.example.thialyson.mymesseger.util.Base64Util;
import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.example.thialyson.mymesseger.util.PreferencesUtil;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;

public class ConversationActivity extends AppCompatActivity {

    private Toolbar toolbar;
    private EditText editText;
    private ListView listView;
    private FloatingActionButton fab;
    private DatabaseReference firebase;
    private ArrayList<Message> messages;
    private MessageAdapter adapter;
    private ValueEventListener valueEventListenerMessage;

    private String nameUserToSend, emailUserToSend, idCurrentUser, idUserToSend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_conversation);

        fab = (FloatingActionButton) findViewById(R.id.btSendMessage);
        toolbar = (Toolbar) findViewById(R.id.toolbarConversation);
        editText = (EditText) findViewById(R.id.etInput);
        listView = (ListView) findViewById(R.id.lvConversations);

        idCurrentUser = PreferencesUtil.getPref("idUser", this);

        setExtras();

        toolbar.setTitle(nameUserToSend);
        setSupportActionBar(toolbar);

        setAdapter();

        setFirebaseInstance();

        listenClickBtSend();
    }

    void setAdapter() {
        messages = new ArrayList<>();
        adapter = new MessageAdapter(this, messages);
        listView.setAdapter(adapter);
    }

    void setFirebaseInstance() {
        firebase = ConfigFirebase.getFirebase().child("messages").child(idCurrentUser).child(idUserToSend);
        setEventListener();
        firebase.addValueEventListener(valueEventListenerMessage);
    }

    void setEventListener() {
        valueEventListenerMessage = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                messages.clear();

                for (DataSnapshot data : dataSnapshot.getChildren()) {
                    Message message = data.getValue(Message.class);
                    messages.add(message);
                }
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        };
    }

    void setExtras() {
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            nameUserToSend = extra.getString("name");
            emailUserToSend = extra.getString("email");
            idUserToSend = Base64Util.encode(emailUserToSend);
        }
    }

    void listenClickBtSend() {
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (editText.getText() != null) {
                    Message message = returnMessage(new Message());


                    boolean resultSent = sendMessage(idCurrentUser, idUserToSend, message);

                    if (!resultSent) {
                        Toast.makeText(ConversationActivity.this, "Erro ao enviar a mensagem", Toast.LENGTH_SHORT).show();

                    } else {
                        boolean resultSentToSend = sendMessage(idUserToSend, idCurrentUser, message);
                        if (!resultSentToSend) {
                            Toast.makeText(ConversationActivity.this, "Erro ao enviar a mensagem ao destinatário", Toast.LENGTH_SHORT).show();
                        }
                    }


                    Conversation userConversation = returnUserConversation(new Conversation(), message);
                    boolean resultSave = saveConversation(idCurrentUser, idUserToSend, userConversation);
                    if (!resultSave) {
                        Toast.makeText(ConversationActivity.this, "Erro ao salvar a mensagem", Toast.LENGTH_SHORT).show();
                    } else {

                        Conversation userToSendConversation = returnUserToSendConversation(new Conversation(), message);
                        boolean resultSaveToSend = saveConversation(idUserToSend, idCurrentUser, userToSendConversation);
                        if (!resultSaveToSend) {
                            Toast.makeText(ConversationActivity.this, "Erro ao salvar a mensagem para o destinatário", Toast.LENGTH_SHORT).show();
                        }
                    }

                    editText.setText("");
                } else {
                    Toast.makeText(ConversationActivity.this, "Digite uma mensagem!", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private Message returnMessage(Message message) {
        message.setUserId(idCurrentUser);
        message.setMessage(editText.getText().toString());
        return message;
    }

    private Conversation returnUserConversation(Conversation conversation, Message message) {
        conversation.setUserId(idUserToSend);
        conversation.setMessage(message.getMessage());
        conversation.setName(nameUserToSend);
        return conversation;
    }

    private Conversation returnUserToSendConversation(Conversation conversation, Message message) {
        conversation.setUserId(idCurrentUser);
        conversation.setMessage(message.getMessage());
        conversation.setName(PreferencesUtil.getPref("nameUser", this));
        return conversation;
    }

    private boolean sendMessage(String idCurrentUser, String idUserToSend, Message message) {

        try {
            firebase = ConfigFirebase.getFirebase().child("messages");
            firebase.child(idCurrentUser).child(idUserToSend).push().setValue(message);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    private boolean saveConversation(String idCurrentUser, String idUserToSend, Conversation conversation) {

        try {
            firebase = ConfigFirebase.getFirebase().child("conversations");
            firebase.child(idCurrentUser).child(idUserToSend).setValue(conversation);
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        firebase.removeEventListener(valueEventListenerMessage);
    }
}
