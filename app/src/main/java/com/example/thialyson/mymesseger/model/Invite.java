package com.example.thialyson.mymesseger.model;

/**
 * Created by Thialyson on 20/01/2018.
 */

public class Invite {

    private Integer status; // 0 free , 1 ocouped, 2 payed.
    private boolean hasAccepted;
    private Long idDoctor;
    private Long idPatient;

    public Invite() {

    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public boolean isHasAccepted() {
        return hasAccepted;
    }

    public void setHasAccepted(boolean hasAccepted) {
        this.hasAccepted = hasAccepted;
    }

    public Long getIdDoctor() {
        return idDoctor;
    }

    public void setIdDoctor(Long idDoctor) {
        this.idDoctor = idDoctor;
    }

    public Long getIdPatient() {
        return idPatient;
    }

    public void setIdPatient(Long idPatient) {
        this.idPatient = idPatient;
    }
}
