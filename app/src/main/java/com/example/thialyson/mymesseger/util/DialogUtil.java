package com.example.thialyson.mymesseger.util;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;

import com.example.thialyson.mymesseger.R;

/**
 * Created by thialyson on 23/04/17.
 */

public class DialogUtil {

    public static AlertDialog.Builder createDialog(Context context, String message) {
        return new AlertDialog.Builder(context, R.style.DialogTheme)
                .setCancelable(false)
                .setMessage(message);
    }

    public static AlertDialog.Builder getNegativeButton(AlertDialog.Builder builder) {
        builder.setNegativeButton("Cancelar", getClickListener());
        return builder;
    }

    public static AlertDialog.Builder getPositiveButton(AlertDialog.Builder builder) {
        builder.setPositiveButton("OK", getClickListener());
        return builder;
    }

    private static DialogInterface.OnClickListener getClickListener() {
        return new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        };
    }

}
