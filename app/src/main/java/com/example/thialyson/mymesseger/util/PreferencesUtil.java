package com.example.thialyson.mymesseger.util;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by thialyson on 22/04/17.
 */

public class PreferencesUtil {

    public static void setPref(String key, String value, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, value);
        editor.apply();
    }

    public static String getPref(String key, Context context) {
        SharedPreferences sharedPref = context.getSharedPreferences("UserData", Context.MODE_PRIVATE);
        return sharedPref.getString(key, "");
    }
}
