package com.example.thialyson.mymesseger;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.thialyson.mymesseger.model.Message;
import com.example.thialyson.mymesseger.util.PreferencesUtil;

import java.util.List;

/**
 * Created by thialyson on 24/05/17.
 */

public class MessageAdapter extends BaseAdapter {

    private List<Message> messages;
    private Context context;

    public MessageAdapter(Context context, List<Message> messages) {
        this.messages = messages;
        this.context = context;
    }

    @Override
    public int getCount() {
        return messages.size();
    }

    @Override
    public Object getItem(int position) {
        return messages.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private class MyViewHolder {
        TextView messageText;

        MyViewHolder(View v) {
            messageText = (TextView) v.findViewById(R.id.messageText);
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Message message = messages.get(position);
        View row = convertView;
        MessageAdapter.MyViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (PreferencesUtil.getPref("idUser", context).equals(message.getUserId())) {
                row = inflater.inflate(R.layout.item_message_right, parent, false);
            } else {
                row = inflater.inflate(R.layout.item_message, parent, false);
            }
            holder = new MessageAdapter.MyViewHolder(row);
            row.setTag(holder);
        } else {
            holder = (MessageAdapter.MyViewHolder) row.getTag();
        }


        holder.messageText.setText(message.getMessage());

        return row;
    }
}
