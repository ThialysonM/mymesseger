package com.example.thialyson.mymesseger;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Handler;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

import com.budiyev.android.circularprogressbar.CircularProgressBar;
import com.example.thialyson.mymesseger.model.Invite;
import com.example.thialyson.mymesseger.model.Message;
import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class InviteActivity extends AppCompatActivity {


    public static final int NOTIFY_ID = 100;
    public NotificationManager notificationManager;

    public static InviteActivity currentActivity;

    private ValueEventListener valueEventListener;
    private DatabaseReference firebase;
    private Button btSend;
    private Switch aSwitch;
    private CircularProgressBar progressBar;
    private TextView textCount;
    private int count;

    boolean isDoctor = true;

    int doctorId = 91, patientId = 2;

    Invite currentInvite;

    Runnable run;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);

        currentActivity = this;

        btSend = (Button) findViewById(R.id.btSendInvite);
        aSwitch = (Switch) findViewById(R.id.switcher);

        textCount = (TextView) findViewById(R.id.textCount);


        progressBar = (CircularProgressBar) findViewById(R.id.progressBar);
        progressBar.setProgress(0);


        final Handler hand = new Handler();
        run = new Runnable() {

            @Override
            public void run() {
                if (count == 30) { //will end if count reach 30 which means 30 second
                    hand.removeCallbacks(run);
                    textCount.setText("acabou");
                } else {
                    count += 1; //add 1 every second
                    progressBar.setProgress(count);
                    textCount.setText(count + "");
                    hand.postDelayed(run, 1000); //will call the runnable after 1 second

                }
            }
        };
        hand.postDelayed(run, 1000);

        sendBtClick();
        setEventListener();

        notificationManager = (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

    }

    void setEventListener() {
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                currentInvite = dataSnapshot.getValue(Invite.class);

                if (isDoctor) {

                    if (currentInvite != null && currentInvite.getStatus() == 0) {

                        showActionButtonsNotification();
                    }
                }

                if (!isDoctor) {
                    if (currentInvite != null && currentInvite.getStatus() == 1) {

                        showActionButtonsNotification();
                    }
                }


                if (currentInvite != null && currentInvite.getStatus() == 2) {
                    Toast.makeText(InviteActivity.this, "Parabéns Consulta agendada.", Toast.LENGTH_SHORT).show();
                }


//                for (DataSnapshot data : dataSnapshot.getChildren()) {
//                    Invite invite2 = data.getValue(Invite.class);
//
//                    if (aSwitch.isChecked()) {
//                        Toast.makeText(InviteActivity.this, "" + invite.getMessage(), Toast.LENGTH_SHORT).show();
//                    }
//                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

                Log.d("", "onCancelled: ");
            }
        };

        setFirebaseInstance();
    }

    void setFirebaseInstance() {
        if (isDoctor) {

            firebase = ConfigFirebase.getFirebase()
                    .child("invites")
                    .child(doctorId + ""); // id doctor
        } else {

            firebase = ConfigFirebase.getFirebase()
                    .child("invites")
                    .child(doctorId + "");
        }


        firebase.addValueEventListener(valueEventListener);
    }


    void sendBtClick() {
        btSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

//                showActionButtonsNotification();

                sendMessage();
            }
        });
    }

    public void sendMessage() {

        if (currentInvite == null) {
            currentInvite = new Invite();
            currentInvite.setStatus(0);
            if (isDoctor)
                currentInvite.setIdDoctor((long) doctorId);
            else
                currentInvite.setIdPatient((long) patientId);
        }

        try {
            firebase = ConfigFirebase.getFirebase().child("invites").child(doctorId + "");
            firebase.setValue(currentInvite);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private Intent getNotificationIntent() {
        Intent intent = new Intent();
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        return intent;
    }

    private PendingIntent getIntentAction(String action) {
        Intent yesReceive = new Intent();
        yesReceive.setAction(action);
        return PendingIntent.getBroadcast(this, 12345, yesReceive, PendingIntent.FLAG_UPDATE_CURRENT);
    }

    private void showActionButtonsNotification() {

        Uri path = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);

        long[] v = {500, 1000};

        Notification notification = new NotificationCompat.Builder(this)
                .setContentIntent(PendingIntent.getActivity(this, 0, getNotificationIntent(), PendingIntent.FLAG_UPDATE_CURRENT))
                .setSmallIcon(R.mipmap.ic_launcher)
                .setTicker("Action Buttons Notification Received")
                .setContentTitle("Solicitação Atendimento")
                .setContentText("Texto secundário")
                .setWhen(System.currentTimeMillis())
                .setSound(path)
                .setVibrate(v)
                .setAutoCancel(false)
                .addAction(android.R.drawable.ic_menu_close_clear_cancel, "Cancelar", getIntentAction("NO_ACTION"))
                .addAction(android.R.drawable.checkbox_on_background, "Aceitar", getIntentAction("YES_ACTION"))
                .build();

        notificationManager.notify(NOTIFY_ID, notification);
    }

    public static InviteActivity getCurrentActivity() {
        return currentActivity;
    }

}

class NotificationReceiver extends BroadcastReceiver {

    InviteActivity activity = InviteActivity.getCurrentActivity();

    @Override
    public void onReceive(Context context, Intent intent) {
        String action = intent.getAction();
        if ("YES_ACTION".equals(action)) {

            if (activity.isDoctor && activity.currentInvite.getStatus() == 0) {
                activity.currentInvite.setHasAccepted(true);
                activity.currentInvite.setStatus(1);
            }

            if (!activity.isDoctor && activity.currentInvite.getStatus() == 1) {
                activity.currentInvite.setStatus(2);
            }

            activity.sendMessage();

            Toast.makeText(context, "yeeesss", Toast.LENGTH_SHORT).show();

        } else if ("NO_ACTION".equals(action)) {

            Toast.makeText(context, "noooooo", Toast.LENGTH_SHORT).show();
            activity.notify();

            activity.notificationManager.cancel(InviteActivity.NOTIFY_ID);
        }
    }
}


