package com.example.thialyson.mymesseger.model;

/**
 * Created by thialyson on 24/05/17.
 */

public class Conversation {


    private String userId, name, message;

    public Conversation() {
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
