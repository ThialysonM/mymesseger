package com.example.thialyson.mymesseger;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thialyson.mymesseger.model.User;
import com.example.thialyson.mymesseger.util.Base64Util;
import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.example.thialyson.mymesseger.util.PreferencesUtil;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

public class RegisterActivity extends AppCompatActivity {

    public EditText name, email, password;
    public Button btRegister;
    private User user;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        name = (EditText) findViewById(R.id.etNameRegister);
        email = (EditText) findViewById(R.id.etEmailRegister);
        password = (EditText) findViewById(R.id.etPasswordRegister);
        btRegister = (Button) findViewById(R.id.btRegister);

        listenBtRegisterClick();
    }

    private void listenBtRegisterClick() {
        btRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                user = new User();
                user.setName(name.getText() != null ? name.getText().toString() : "");
                user.setEmail(email.getText() != null ? email.getText().toString() : "");
                user.setPassword(password.getText() != null ? password.getText().toString() : "");

                validateFields();
            }
        });
    }

    private void validateFields() {
        if (!user.getName().equals("") && !user.getEmail().equals("") && !user.getPassword().equals("")) {
            register();
        } else {
            showToast("Todos os campos são obrigatórios!");
        }
    }

    private void register() {

        FirebaseAuth auth = ConfigFirebase.getAuth();
        auth.createUserWithEmailAndPassword(
                user.getEmail(),
                user.getPassword()).addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {

                    String idUser = Base64Util.encode(user.getEmail());
                    PreferencesUtil.setPref("idUser", idUser, RegisterActivity.this);
                    user.setId(idUser);
                    user.save();
                    callHomeActivity();
                } else {
                    resolveExceptions(task);
                }
            }
        });
    }

    private void resolveExceptions(Task<AuthResult> task) {
        try {
            throw task.getException();
        } catch (FirebaseAuthWeakPasswordException e) {
            showToast("Digite uma senha mais forte!");
        } catch (FirebaseAuthInvalidCredentialsException e) {
            showToast("Email inválido!");
        } catch (FirebaseAuthUserCollisionException e) {
            showToast("Este email já está sendo usado no app!");
        } catch (FirebaseException e) {
            showToast("Sem conexão com a internet!");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show();
    }

    private void callHomeActivity(){
        startActivity(new Intent(RegisterActivity.this, MainActivity.class));
        finish();
    }
}
