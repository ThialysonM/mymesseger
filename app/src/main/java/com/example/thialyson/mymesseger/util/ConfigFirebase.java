package com.example.thialyson.mymesseger.util;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by thialyson on 22/04/17.
 */

public final class ConfigFirebase {

    private static DatabaseReference reference;
    private static FirebaseAuth auth;

    public static DatabaseReference getFirebase() {
        if (reference == null) {
            reference = FirebaseDatabase.getInstance().getReference();
        }
        return reference;
    }

    public static FirebaseAuth getAuth() {
        if (auth == null) {
            auth = FirebaseAuth.getInstance();
        }
        return auth;
    }

}
