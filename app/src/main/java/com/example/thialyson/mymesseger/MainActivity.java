package com.example.thialyson.mymesseger;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.Toast;

import com.example.thialyson.mymesseger.fragments.ContactsFragment;
import com.example.thialyson.mymesseger.fragments.ConversationsFragment;
import com.example.thialyson.mymesseger.model.Contact;
import com.example.thialyson.mymesseger.model.User;
import com.example.thialyson.mymesseger.util.Base64Util;
import com.example.thialyson.mymesseger.util.ConfigFirebase;
import com.example.thialyson.mymesseger.util.DialogUtil;
import com.example.thialyson.mymesseger.util.PreferencesUtil;
import com.example.thialyson.mymesseger.util.ViewPagerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ValueEventListener;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth auth;
    private Toolbar toolbar;
    public ViewPager viewPager;
    public TabLayout tabLayout;
    public EditText emailNewContact;

    public DatabaseReference refFirebase;

    public String idContact;

    public ViewPagerAdapter viewPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);

        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPagerAdapter.addFragments(new ConversationsFragment(), "Conversas");
        viewPagerAdapter.addFragments(new ContactsFragment(), "Contatos");
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout.setupWithViewPager(viewPager);

        setSupportActionBar(toolbar);
        auth = ConfigFirebase.getAuth();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.search:
                return true;
            case R.id.addUser:
                addContact();
                return true;
            case R.id.config:
                return true;
            case R.id.logout:
                logout();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void logout() {
        auth.signOut();
        callLoginActivity();
    }

    private void callLoginActivity() {
        startActivity(new Intent(this, LoginActivity.class));
        finish();
    }

    private void addContact() {

        emailNewContact = new EditText(this);
        emailNewContact.setTextColor(getResources().getColor(R.color.primaryText, null));

        AlertDialog.Builder builder = DialogUtil.createDialog(this, "Digite o email");
        builder.setView(emailNewContact);
        builder = DialogUtil.getNegativeButton(builder)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        setContact();
                    }
                });

        builder.create().show();
    }

    private void setContact() {

        if (emailNewContact.getText() != null) {

            idContact = Base64Util.encode(emailNewContact.getText().toString());
            refFirebase = ConfigFirebase.getFirebase().child("users").child(idContact);

            refFirebase.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    if (dataSnapshot.getValue() != null) {
                        saveContact(dataSnapshot);
                    } else {
                        Toast.makeText(MainActivity.this, "Usuário não possui cadastro", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        } else {
            Toast.makeText(MainActivity.this, "Preencha o email", Toast.LENGTH_SHORT).show();
        }
    }

    private void saveContact(DataSnapshot dataSnapshot) {
        String userId = PreferencesUtil.getPref("idUser", MainActivity.this);
        User user = dataSnapshot.getValue(User.class);

        Contact contact = new Contact();
        contact.setUserId(userId);
        contact.setEmail(user.getEmail());
        contact.setName(user.getName());

        refFirebase = ConfigFirebase.getFirebase();
        refFirebase.child("contacts")
                .child(userId)
                .child(idContact)
                .setValue(contact);

        Toast.makeText(MainActivity.this, "Contato cadastrado!", Toast.LENGTH_SHORT).show();
    }


}
